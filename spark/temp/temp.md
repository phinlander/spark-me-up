### Making a home temperature sensor
------------------------------------

Once again, this guide brought to you by [Spark.io's examples section](http://docs.spark.io/examples/#measuring-the-temperature). From the guide:

>We have now learned how to send custom commands to the Core and control the hardware. But how about reading data back from the Core?

>In this example, we will hook up a temperature sensor to the Core and read the values over the internet with a web browser.

>We have used a widely available analog temperature sensor called TMP36 from Analog Devices, and is the temperature sensor that comes with your Spark Maker Kit! You can download the datasheet here.

>Notice how we are powering the sensor from 3.3V\* pin instead of the regular 3.3V. This is because the 3.3V\* pin gives out a (LC) clean filtered voltage, ideal for analog applications like these. If the readings you get are noisy or inconsistent, add a 0.01uF (10nF) ceramic capacitor between the analog input pin (in this case, A7) and GND as shown in the set up. Ideally, the sensor should be placed away from the Core so that the heat dissipated by the Core does not affect the temperature readings.