// Reading the temperature
int pin = A7;
double temp = 0.0;

void setup() {
    // Set the Spark variable
    Spark.variable("temp", &temp, DOUBLE);
    
    // Set the pin to input mode
    pinMode(pin, INPUT);
}

void loop() {
    int reading = 0;
    double voltage = 0.0;
    
    // Keep reading the sensor value so when we make an API
    // call to read its value, we have the latest one
    reading = analogRead(pin);
    
    // The returned value from the Core is going to be in the range
    // from 0 to 4095 (4096 = 4 * 2^10)
    // Calculate the voltage from the sensor reading
    voltage = (reading * 3.3) / 4095;
    
    // Calculate the temperature and update our static variable
    temp = (voltage - 0.5) * 100;
    // To get the temp in Farenheit use the following
    temp = temp*(9/5)+32;
}