//-----------------
// Control a servo
// using the
// temperature
//-----------------

// Servo
Servo servo;
int servopin = A0;
int servopos = 0;

// Temp
int temppin = A7;
double tempval = 0.0;
double tempmin = 22.0;
double tempmax = 28.0;

// Time variable
unsigned long lastTime = 0UL;

// Buffer variable
char publishString[5];

// Warning buzzer
int buzzin = D0;

void setup() {
    servo.attach(servopin);
    servo.write(servopos);
    
    pinMode(temppin, INPUT);
    
    pinMode(buzzin, OUTPUT);
}

void loop() {
    // Get the temperature
    int reading = 0;
    double voltage = 0.0;
    
    // Keep reading the sensor value so when we make an API
    // call to read its value, we have the latest one
    reading = analogRead(A7);
    
    // The returned value from the Core is going to be in the range
    // from 0 to 4095 (4096 = 4 * 2^10)
    // Calculate the voltage from the sensor reading
    voltage = (reading * 3.3) / 4095;
    
    // Calculate the temperature and update our static variable
    tempval = (voltage - 0.5) * 100;
    
    // Set servo position based on temp
    if (tempval < tempmin) {
        servopos = 0;
        digitalWrite(buzzin, LOW);
    } else if (tempval > tempmax) {
        servopos = 180;
        digitalWrite(buzzin, HIGH);
        
    } else {
        servopos = 180 - 180*(tempmax - tempval)/(tempmax-tempmin);
        digitalWrite(buzzin, LOW);
    }
    
    // Set servo position
    servo.write(servopos);
    
    // Every 1 second publish the temperature
    unsigned long now = millis();
    if(now-lastTime>1000UL) {
        lastTime = now;
        
        sprintf(publishString, "%2.2f",tempval);
        Spark.publish("temp",publishString);
    }
}