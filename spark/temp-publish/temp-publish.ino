// Reading the temperature and lighting up
// the right LEDs

// Temperature variable
double temp = 0.0;

// Led variables
int ledred = D0;
int ledgre = D1;

// Time variable
unsigned long lastTime = 0UL;

// Buffer variable
char publishString[5];

void setup() {
    // Set A7 to read in temp
    pinMode(A7, INPUT);
    
    // Set D0 to control red LED then turn off
    pinMode(ledred, OUTPUT);
    digitalWrite(ledred, LOW);
    
    // Set D1 to control green LED then turn off
    pinMode(ledgre, OUTPUT);
    digitalWrite(ledgre, LOW);
}

void loop() {
    int reading = 0;
    double voltage = 0.0;
    
    // Keep reading the sensor value so when we make an API
    // call to read its value, we have the latest one
    reading = analogRead(A7);
    
    // The returned value from the Core is going to be in the range
    // from 0 to 4095 (4096 = 4 * 2^10)
    // Calculate the voltage from the sensor reading
    voltage = (reading * 3.3) / 4095;
    
    // Calculate the temperature and update our static variable
    temp = (voltage - 0.5) * 100;
    
    // Control which LED is on
    if (temp > 20.0) {
        digitalWrite(ledred, LOW);
        digitalWrite(ledgre, HIGH);
    } else {
        digitalWrite(ledred, HIGH);
        digitalWrite(ledgre, LOW);
    }
    
    // Every 1 second publish the temperature
    unsigned long now = millis();
    if(now-lastTime>1000UL) {
        lastTime = now;
        
        sprintf(publishString, "%2.2f",temp);
        Spark.publish("temp",publishString);
    }
}