### "My first RESTful app" for Makers
-------------------------------------

Now that we know how to control simple LED's, let's start putting Spark's WiFi to use! This tutorial was originally found in [Spark.io's examples section](http://docs.spark.io/examples/#blink-an-led)

>Now that we know how to blink an LED, how about we control it over the Internet? This is where the fun begins.

>Lets hook up two LEDs this time.Here is the algorithm:

>- Set up the pins as outputs that have LEDs connected to them
>- Create and register a Spark function ( this gets called automagically when you make an API request to it)
>- Parse the incoming command and take appropriate actions
