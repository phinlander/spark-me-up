// Pin D0 for led
int led0 = D0;
int led1 = D1;

void setup() {
  Spark.function("twinled",ledControl);

  // Initialize D0 and D1 pin for output
  pinMode(led0, OUTPUT);
  pinMode(led1, OUTPUT);

  // Turn them off
  digitalWrite(led0, LOW);
  digitalWrite(led1, LOW);
}

void loop() {

}

int ledControl(String command) {
  // Convert from ascii to integer
  int pinNumber = (command.charAt(0) - '0');

  // Make sure only pins 0 and 1 get commands
  if (pinNumber < 0 || pinNumber > 1) return pinNumber;

  // Swap from HIGH to LOW or LOW to HIGH
  int state = HIGH;
  if (digitalRead(pinNumber) == HIGH) state = LOW;

  // Send the command to the LED
  digitalWrite(pinNumber, state);

  // Success!
  return 1;
}