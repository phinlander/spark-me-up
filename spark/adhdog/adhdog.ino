Servo servo;

int FORWARD = 1;
int BACKWARD = -1;
int BASE = 90;

int LED = D0;
int BEEP = D1;

void setup() {
    // Send a beep and make it crank
    Spark.function("run",run);
    
    // Initialize D0 pin for output
    pinMode(LED, OUTPUT);
    
    // Initialize D1 pin for output
    pinMode(BEEP, OUTPUT);

    // Turn it off
    digitalWrite(LED, LOW);
}

int speed(float speed, int direction) {
    return BASE + BASE*speed*direction;
}

// Bleet bleeter
void beep() {
    digitalWrite(BEEP, HIGH);
    delay(150);
    digitalWrite(BEEP, LOW);
}

void loop() {
    // Do nothing, no loop stupid
}

int run(String command) {
    // Turn on the LED
    digitalWrite(LED, HIGH);
    
    // Bleet bleeter
    beep();
    delay(100);
    beep();
    delay(100);
    beep();
    delay(100);
    beep();
    
    // Wait two seconds then run the grinder
    delay(1000);
    
    // Attach the servo
    servo.attach(A7);
    // Start the thing running
    servo.write(speed(1, BACKWARD));
    // Let it spin for half a second
    delay(1000);
    
    // Turn off the LED
    digitalWrite(LED, LOW);
    // Spinning stopped
    servo.write(speed(0,BACKWARD));
    // Detach the servo
    servo.detach();

    // Success!
    return 1;
}