// Reading the temperature and lighting up
// the right LEDs

// Temperature variable
double temp = 0.0;

// Led variables
int ledred = A0;
int ledgre = D0;
int ledblu = D1;

// Temp levels
int templow = 22;
int tempmed = 24;
int temphig = 26;

// Time variable
unsigned long lastTime = 0UL;

// Buffer variable
char publishString[5];

void setup() {
    // Set A7 to read in temp
    pinMode(A7, INPUT);
    
    // Set D0 to control red LED then turn off
    pinMode(ledred, OUTPUT);
    analogWrite(ledred, 255);
    
    // Set D1 to control green LED then turn off
    pinMode(ledgre, OUTPUT);
    analogWrite(ledgre, 0);
    
    pinMode(ledblu, OUTPUT);
    analogWrite(ledblu, 255);
}

void loop() {
    int reading = 0;
    double voltage = 0.0;
    
    // Keep reading the sensor value so when we make an API
    // call to read its value, we have the latest one
    reading = analogRead(A7);
    
    // The returned value from the Core is going to be in the range
    // from 0 to 4095 (4096 = 4 * 2^10)
    // Calculate the voltage from the sensor reading
    voltage = (reading * 3.3) / 4095;
    
    // Calculate the temperature and update our static variable
    temp = (voltage - 0.5) * 100;
    
    int bright_red = 255;
    int bright_gre = 255;
    int bright_blu = 255;
    
    // Control LED color
    
    if( temp < templow ) {
        bright_blu = 255;
    } else if( temp >= templow && temp <= tempmed ) {
        bright_blu = 255 * (1- (tempmed - temp)/2);
        bright_gre = 255 * (tempmed - temp)/2;
    } else if( temp > tempmed && temp <= temphig ) {
        bright_gre = 255 * (1 - (temphig - temp)/2);
        bright_red = 255 * (temphig - temp)/2;
    } else {
        bright_red = 0;
    }
    
    
    analogWrite(ledred, bright_red);
    analogWrite(ledgre, bright_gre);
    analogWrite(ledblu, bright_blu);
    
    // Every 1 second publish the temperature
    unsigned long now = millis();
    if(now-lastTime>1000UL) {
        lastTime = now;
        
        sprintf(publishString, "%2.2f",temp);
        Spark.publish("temp",publishString);
    }
}