### "Hello, World!" for Makers
------------------------------

This is the most basic and necessary of examples, as it is used to learn the basics of the breadboard and Spark, as well as check to see if everything is set up correctly. This tutorial was originally found in [Spark.io's examples section](http://docs.spark.io/examples/#blink-an-led)

>For this example, you will need a Spark Core (duh!), a Breadboard, an LED, a Resistor (we will soon find out a suitable value) and a USB cable.

>Connect everything together as shown in the picture. The LED is connected to pin D0 of the Core. The positive (longer pin) of the LED is connected to D0 via a resistor and its negative pin (shorter) is connected to ground.

>One LED setupBut wait, what's the value of the resistor again? *Here's how we find that out:*

>According to Ohm's Law : Voltage = Current x Resistance

>Therefore, Resistance = Voltage/ Current

>In our case, the output voltage of the Core is 3.3V but the LED (typically) has a forward voltage drop of around 2.0V. So the actual voltage would be:3.3V - 2.0V = 1.3V

>The required current to light up an LED varies any where between 2mA to 20mA. More the current, brighter the intensity. But generally its a good idea to drive the LED at a lower limit to prolong its life span. We will choose a drive current of 5mA.

>Hence, Resistance = 1.3V/ 5mA = 260 Ohms

>*NOTE: Since there is so much variation in the values of the forward voltage drop of the LEDs depending upon type, size, color, manufacturer, etc., you could successfully use a resistor value from anywhere between 220Ohms to 1K Ohms.*

>In the picture above, we used a 1K resistor (Brown Black Red)

In case you run into trouble, make sure and check Spark's [troubleshooting guide](http://docs.spark.io/troubleshooting/). If you are like me you may have some issues with keeping a solid connection to your router. Make sure your router is outputing a signal in the b/g band, between channel 1 and 10, and at a 20Hz range, as it will fail to connect at 40Hz.

If you don't see any problems enter the code below and do an over-the-air firmware update!