// Pin D0 for led
int led0 = D0;

void setup() {
  // Initialize D0 pin for output
  pinMode(led0, OUTPUT);

  // Turn it off
  digitalWrite(led0, LOW);
}

void loop() {
  // Turn the LED on
  digitalWrite(led0, HIGH);
  delay(1000);

  // Turn the LED off
  digitalWrite(led0, LOW);
  delay(1000);
}