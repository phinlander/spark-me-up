var temp = temp || {};

temp.create = function(main) {
  var name = 'Temperature Monitor';
  var id = 'temp';
  var youtube = '//www.youtube.com/embed/eu4aWt4TRUs';
  
  // Spark web controls
  var controls = tools.row();
  
  var command_col = tools.col('col-md-4');
  var command_label = tools.label('Command');
  var command_input = tools.input('text',{placeholder:id});
  command_col.append(command_label);
  command_col.append(command_input);
  
  var button_col = tools.col('col-md-4',{role:'group'},['btn-group']);
  var button_tmp = tools.button("Temp",{},['twinled-button','btn-default']);
  button_col.append(button_tmp);

  var output_col = tools.col('col-md-4');
  
  var getParams = function(target) {
    return $(target).attr('id');
  }
  
  var onSuccess = function(msg) {
    console.log(msg);
    output_col.html(tools.success('Current temperature: '+msg.result));
  }
  
  var onError = function(msg) {
    console.log(msg);
    var error = JSON.parse(msg.responseText);
    output_col.html(tools.error('SERVER ERROR<br/>'+
                            '<br/>Code: '+error.code+
                            '<br/>Error: '+error.error+
                            '<br/>Error Description: '+error.error_description));
  };
  
  tools.setGet(button_tmp,command_input,getParams,output_col,onSuccess,onError);

  controls.append(command_col);
  controls.append(button_col);
  controls.append(output_col);
  
  tools.makeTutorial(main,name,id,youtube,controls);
}