var tools = tools || {};

tools.setAttr = function(e, attr) {
  if(attr) e.attr(attr);
  return e;
}

tools.setClasses = function(e, classes) {
  if(Array.isArray(classes)) {
    $.each(classes,function(k,v) {
      e.addClass(v);
    })
  }
  return e;
}

tools.element = function(type,attr,classes) {
  var e = $(type);
  this.setAttr(e,attr);
  this.setClasses(e,classes);
  return e;
}

tools.div = function(attr,classes) {
  var e = this.element('<div>',attr,classes);
  return e;
}

tools.row = function(attr, classes) {
  var e = this.element('<div>',attr,classes);
  e.addClass('row');
  return e;
}

tools.col = function(size, attr, classes) {
  var e = this.element('<div>',attr,classes);
  e.addClass(size);
  return e;
}

tools.li = function(attr,classes) {
  var e = this.element('<li>',attr,classes);
  return e;
}

tools.a = function(href,text,attr,classes) {
  var e = this.element('<a>',href,classes);
  e.attr({href:href});
  e.html(text);
  return e;
}

tools.h1 = function(text,attr,classes) {
  var e = this.element('<h1>',attr,classes);
  e.html(text);
  return e;
}

tools.h3 = function(text,attr,classes) {
  var e = this.element('<h3>',attr,classes);
  e.html(text);
  return e;
}

tools.label = function(text,attr,classes) {
  var e = this.element('<label>',attr,classes);
  e.html(text);
  return e;
}

tools.input = function(inputtype,attr,classes) {
  var e = this.element('<input type="'+inputtype+'">',attr,classes);
  return e;
}

tools.button = function(text, attr, classes) {
  classes.push('btn');
  var e = this.element('<button type="button">',attr,classes);
  e.html(text);
  return e;
}

tools.p = function(text,attr,classes) {
  var e = this.element('<p>',attr,classes);
  e.html(text);
  return e;
}

tools.small = function(text,attr,classes) {
  var e = this.element('<small>',attr,classes);
  e.html(text);
  return e;
}

tools.hr = function() {
  var e = this.element('<hr>');
  return e;
}

// Spark me up! specific

tools.addNav = function(name,id) {
  $('#navbar ul').append(this.navLink(name,id));
}

tools.navLink = function(name,id) {
  var li = this.li();
  var a = this.a('#'+id,name);
  a.click(this.internalLink);
  li.append(a);
  return li;
}

tools.title = function(text) {
  var e = this.h1(text);
  var home = this.a('#','HOME');
  home.click(this.scrollTop);
  e.append(this.small().append(home));
  return e;
}

tools.output = function(text) {
  var e = this.p(text);
  e.addClass('spark-output');
  return e;
}

tools.status = function(text) {
  var e = this.output(text);
  e.addClass('spark-status');
  return e;
}

tools.success = function(text) {
  var e = this.output(text);
  e.addClass('spark-success');
  return e;
}

tools.error = function(text) {
  var e = this.output(text);
  e.addClass('spark-error');
  return e;
}

// Make tutorial modules

tools.getRoot = function(id,ext) {
  return '../spark/'+id+'/'+id+'.'+ext;
}

tools.getDevice = function() {
  return $('#device-id').val();
}

tools.getToken = function() {
  return $('#access-token').val();
}

tools.scrollTop = function(event) {
  event.preventDefault();
  $('html, body').animate({scrollTop: 0}, 'fast');
}

tools.internalLink = function(event) {
  event.preventDefault();
  $('html, body').animate({scrollTop: $(event.target.hash).offset().top-60}, 'fast');
}

tools.getPNG = function(id) {
  var png_div = this.div({},['embed-responsive','embed-responsive-16by9']);
  var png_image = this.element('<img>',{src:this.getRoot(id,'png')},['embed-responsive-item']);
  png_div.append(png_image);
  return png_div;
}

tools.youtube = function(source) {
  var youtube_div = this.div({},['embed-responsive','embed-responsive-16by9']);
  var youtube_iframe = this.element('<iframe allowfullscreen>',{src:source,frameborder:0},['embed-responsive-item']);
  youtube_div.append(youtube_iframe);
  return youtube_div;
}

tools.getMD = function(id,callback) {
  $.get(this.getRoot(id,'md'),callback);
}

tools.getCode = function(id,callback) {
  $.get(this.getRoot(id,'ino'),callback);
}

tools.reCode = /\n/gi

tools.parseCode = function(text) {
  var pre = this.element('<pre>',{},['prettyprint']);
  var code = this.element('<code>',{},['language-c']);
  code.html(text.replace(this.reCode,'<br/>'));
  pre.append(code);
  return pre;
}

tools.setGet = function(target,command_input,getParams,output,onSuccess,onError) {
  target.click(function(event) {
    var sparkParams = tools.getSparkParams(command_input,output);
    if(sparkParams == -1) return;
    var params = getParams(event.target);
    output.html(tools.status('Sending command to the server...'));

    $.ajax({
      url: "https://api.spark.io/v1/devices/"+sparkParams.device_id+"/"+sparkParams.command,
      contentType: 'application/x-www-form-urlencoded',
      type: 'GET',
      data: "access_token="+sparkParams.access_token+"&params="+params
    }).done(onSuccess).error(onError)
  })
}

tools.setPost = function(target,command_input,getParams,output,onSuccess,onError) {
  target.click(function(event) {
    var sparkParams = tools.getSparkParams(command_input,output);
    if(sparkParams == -1) return;
    var params = getParams(event.target);
    output.html(tools.status('Sending command to the server...'));

    $.ajax({
      url: "https://api.spark.io/v1/devices/"+sparkParams.device_id+"/"+sparkParams.command,
      contentType: 'application/x-www-form-urlencoded',
      type: 'POST',
      data: "access_token="+sparkParams.access_token+"&params="+params
    }).done(onSuccess).error(onError)
  })
}

tools.getSparkParams = function(command_input,output) {
  var device_id = tools.getDevice();
  var access_token = tools.getToken();
  if (device_id === "" || access_token === "") {
    output.html(tools.error('Please ensure Device_id and Access_token have values'));
    return -1;
  }

  var command = command_input.val();
  if (command === "") {
    output.html(tools.error('Please enter a command to send to your Spark'));
    return -1;
  }
  
  return {device_id:device_id,access_token:access_token,command:command};
}

tools.makeTutorial = function(main,name,id,source,controls) {
  // Add a link in the navbar
  this.addNav(name,id);
  
  // Make the container div
  var container = this.div({id:id});
  
  // Tutorial title
  var title = this.title(name.toUpperCase()+' ');
  
  // Tutorial text/code
  var guide = this.row();
  
  // Guide olumn
  var guide_text_col = this.col('col-md-6');
  this.getMD(id,function(data) {
    guide_text_col.append(markdown.toHTML(data));
  });
  guide.append(guide_text_col);
  
  // Media column
  var media_col = this.col('col-md-6');
  
  // Breadboard image
  var image = this.getPNG(id);
  media_col.append(image);
  media_col.append(this.hr());
  
  // Code
  var code_div = this.div();
  this.getCode(id,function(data) {
    code_div.append(tools.parseCode(data));
    prettyPrint();
  });
  media_col.append(code_div);
  media_col.append(this.hr());
  
  // Youtube embed
  var youtube = this.youtube(source);
  media_col.append(youtube);
  
  // Add content to the media column
  guide.append(media_col);
  
  // Add the components to the container
  container.append(title);
  container.append(guide);
  if(controls) {
    container.append(this.hr());
    container.append(controls);
  }
  
  // Add it to your main content div
  main.append(container);
  main.append(this.hr());
}