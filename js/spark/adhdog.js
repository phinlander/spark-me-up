var adhdog = adhdog || {};

adhdog.create = function(main) {
  var name = 'ADHDog';
  var id = 'adhdog';
  var youtube = '';
  
  // Spark web controls
  var controls = tools.row();
  
  var command_col = tools.col('col-md-4');
  var command_label = tools.label('Command');
  var command_input = tools.input('text',{placeholder:id});
  command_col.append(command_label);
  command_col.append(command_input);
  
  var button_col = tools.col('col-md-4',{role:'group'},['btn-group']);
  var button_run = tools.button("Run",{id:'adhdog-0'},['adhdog-button','btn-success']);
  button_col.append(button_run);

  var output_col = tools.col('col-md-4');
  
  var getParams = function(target) {
    return $(target).attr('id').charAt(8);
  }
  
  var onSuccess = function(msg) {
    console.log(msg);
    if(msg.return_value != 1) {
      output_col.html(tools.error('Spark Error: Return value '+msg.return_value));
    } else {
      output_col.html(tools.success("Success! Check your Spark LED"));
    }
  };
  
  var onError = function(msg) {
    console.log(msg);
    var error = JSON.parse(msg.responseText);
    output_col.html(tools.error('SERVER ERROR<br/>'+
                            '<br/>Code: '+error.code+
                            '<br/>Error: '+error.error+
                            '<br/>Error Description: '+error.error_description));
  };
  
  tools.setPost(button_run,command_input,getParams,output_col,onSuccess,onError);

  controls.append(command_col);
  controls.append(button_col);
  controls.append(output_col);
  
  tools.makeTutorial(main,name,id,youtube,controls);
}